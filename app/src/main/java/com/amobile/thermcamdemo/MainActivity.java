package com.amobile.thermcamdemo;

import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.OrientationEventListener;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.rock.thermcamsdk.OnBitmapListener;
import org.rock.thermcamsdk.Sight;
import org.rock.thermcamsdk.ht08.Ht08;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "ThermCamDemo";
    private ImageView imgCamera;
    private OrientationEventListener orientationListener;
    private Ht08 cam = null;
    private Sight sight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        initObjects();
    }

    private void initUI() {
        imgCamera = (ImageView) findViewById(R.id.cam_im);
    }

    private void initObjects() {
        orientationListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {
                //Log.v(TAG,"Orientation changed to " + orientation);
                if (orientation >= 315 || orientation < 45)
                    sight.setLabelAngle(0);
                else if (orientation >= 45 && orientation < 135)
                    sight.setLabelAngle(90);
                else if (orientation >= 135 && orientation < 225)
                    sight.setLabelAngle(180);
                else if (orientation >= 225 && orientation < 315)
                    sight.setLabelAngle(270);
            }
        };
        cam = new Ht08.Builder(this)
                .build();
        sight = cam.createSight();
        sight.setMode(Sight.Mode.Fixed);
    }

    private synchronized void startThermal() {
        cam.setOnLeptonBitmapListener(new OnBitmapListener() {
            @Override
            public void onReceived(final Bitmap bitmap) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgCamera.setImageBitmap(bitmap);
                    }
                });
            }
        });
        cam.start();
    }

    private synchronized void stopThermal() {
        cam.stop();
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        orientationListener.disable();
        stopThermal();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startThermal();
        orientationListener.enable();
    }
}